Source: namecheap
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ana Custura <ana@netstat.org.uk>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               dh-exec,
               python3-all,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-requests,
Rules-Requires-Root: no
Standards-Version: 4.4.1
Homepage: https://github.com/Bemmu/PyNamecheap/releases
Vcs-Git: https://salsa.debian.org/python-team/packages/namecheap.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/namecheap

Package: python3-namecheap
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-requests
Description: API library for DNS provider Namecheap (Python 3)
 This package provides support for registering a domain, checking domain name
 availability, listing registered domains, getting contact information for a
 domain, setting DNS info to default values and setting DNS host records.
 .
 This package installs the library for Python 3.

Package: namecheap
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-namecheap (= ${binary:Version})
Description: API command line client for DNS provider Namecheap (Python 3)
 This package provides a command line tool which supports registering a domain,
 checking domain name availability, listing registered domains, getting contact
 information for a domain, setting DNS info to default values and setting DNS
 host records.
 .
 This package installs the CLI tool.
